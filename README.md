# **DOCUMENTACION DEL PROYECTO**

![UCV Logo](https://www.ucv.edu.pe/wp-content/themes/ucv/images/LogotipoUCV_VersiónLarga.png)

## **FACULTAD DE INGENIERIA Y ARQUITECTURA**

## **ESCUELA DE IGENIERÍA DE SISTEMAS**

</br>

## **PROYECTO DE ANALISIS Y DISEÑO DE SISTEMAS**

### SISTEMA PARA EL REGISTRO DE CITAS MÉDICAS

### PUESTO DE SALUD NUEVO TRUJILLO

</br>

### **PROFESOR:**

#### BERROCAL NAVARRO RICHARD LEONARDO (0000-0001-8718-3150)

</br>

### **EQUIPO N3**

#### BECERRA MORALES, HARRY ANDERSON

#### CHACON ZOLANO JHONY CRISOLDO (0000-0002-7192-4396)

#### HUARINGA GONZALES, JHOU DEYVI (0000-0001-5267-5489)

#### LAVADO NARIO, BRAULIO RODOLFO (0000-0003-4248-3031)

#### MENDOZA MINCHAN, EDWIN VICTOR (0000-0001-8307-5523)

#### ROJAS CASTILLO, JHEF JHOSEP (0000-0003-2499-8656)

</br>

#### **LIMA - PERÚ**

</br>

#### **2022**

</br>

***

### **PORCENTAJE DE TRABAJO DEL EQUIPO**

| INTEGRANTES | PRIMER INFORME | SEGUNDO INFORME | INFORME FINAL
---|:---:|:---:|:---:
BECERRA MORALES, HARRY ANDERSON | 0% | 0% | 0%
CHACON ZOLANO JHONY CRISOLDO | 40% | 100% | 100%
HUARINGA GONZALES, JHOU DEYVI | 40% | 60% | 95%
LAVADO NARIO, BRAULIO RODOLFO | 40% | 90% | 70%
MENDOZA MINCHAN, EDWIN VICTOR | 100% | 100% | 100%
ROJAS CASTILLO, JHEF JHOSEP | 100% | 100% | 100%
</br>

***

## **INDICE**

[RESUMEN EJECUTIVO](#resumen-ejecutivo)

[INTRODUCCION](#introducción)

[1. ESTUDIO DE FACTIBILIDAD](#1-estudio-de-factibilidad)

[1.1. FACTIBILIDAD OPERATIVA Y TÉCNICA: LA VISIÓN DEL SISTEMA](#11-factibilidad-operativa-y-técnica-la-visión-del-sistema)

[2. MODELO DE NEGOCIO](#2-modelo-del-negocio)

[2.1. MODELO DE CASO DE USO DEL NEGOCIO](#21-modelo-de-caso-de-uso-del-negocio)

[2.1.1. LISTA DE LOS ACTORES DEL NEGOCIO](#211-lista-de-los-actores-del-negocio)

[2.1.2. LISTA DE LOS CASOS DE USO DEL NEGOCIO](#212-lista-de-casos-de-uso-del-negocio)

[2.1.3. DIAGRAMA DE CASOS DE USO DEL NEGOCIO](#213-diagrama-de-casos-de-uso-del-negocio)

[2.1.4. ESPECIFICACIONES DE CASOS DE USO DEL NEGOCIO](#214-especificaciones-de-casos-de-uso-del-negocio)

[2.2. MODELO DE ANÁLISIS DEL NEGOCIO](#22-modelo-de-análisis-de-negocio)

[2.2.1. LISTA DE TRABAJADORES DE NEGOCIO](#221-lista-de-trabajadores-de-negocio)

[2.2.2. LISTA DE ENTIDADES DE NEGOCIO](#222-lista-de-entidades-de-negocio)

[2.2.3. REALIZACIÓN DE CASOS DE USO DEL NEGOCIO](#223-realización-de-casos-de-uso-del-negocio)

[2.2.4. DIAGRAMA DE ACTIVIDADES](#224-diagrama-de-actividades)

[2.2.5. REALIZACIÓN DE CLASES DE DOMINIO](#225-realización-de-clases-de-dominio)

[2.3. GLOSARIO DE TÉRMINOS](#23-glosario-de-términos)

[2.4. REGLAS DE NEGOCIO](#24-reglas-de-negocio)

[3. CAPTURA DE REQUERIMIENTO](#3-captura-de-requerimientos)

[3.1. FUENTES DE OBTENCIÓN DE REQUERIMIENTO](#31-fuentes-de-obtención-de-requerimientos)

[3.1.1. INFORME DE ENTREVISTA O MATERIAL TÉCNICO](#311-informe-de-entrevistas-o-material-técnico)

[3.1.2. MATRIZ DE ACTIVIDADES Y REQUERIMIENTOS](#312-matriz-de-actividades-y-requerimientos)

[3.2. MODELO DE CASO DE USO](#32-modelo-de-casos-de-uso)

[3.2.1. LISTA DE ACTORES DEL SISTEMA](#321-lista-de-actores-del-sistema)

[3.2.2. LISTA DE CASOS DE USO DEL SISTEMA](#322-lista-de-casos-de-uso-del-sistema)

[3.2.3. LISTA DE CASOS DE USO PRIORIZADOS](#323-lista-de-casos-de-uso-priorizados)

[3.2.4. DIAGRAMAS DE CASO DE USO DEL SISTEMA](#324-diagramas-de-casos-de-uso-del-sistema)

[3.2.5. ESPECIFICACIONES DE REQUERIMIENTOS DE SOFTWARE](#325-especificaciones-de-requerimientos-de-software)

[3.2.6. ESPECIFICACIONES DE CASOS DE USO](#326-especificaciones-de-casos-de-uso)

[3.3. PROTOTIPO DE PRINCIPALES INTERFACES](#33-prototipo-de-principales-interfaces)

[3.4. DIAGRAMA DE CLASES DE DISEÑO](#34-diagrama-de-clases-de-diseño)

[3.5. DIAGRAMA DE PAQUETES](#35-diagrama-de-paquetes)

[3.6. DIAGRAMA DE SECUENCIA](#36-diagrama-de-secuencia)

[3.7. DIAGRAMA DE ESTADO](#37-diagrama-de-estado)

[CONCLUSIONES](#conclusiones)

</br>

***

## **RESUMEN EJECUTIVO**

En este trabajo nos enfocamos en el puesto de salud "Nuevo Trujillo", que tiene como especialidades Medicina, Obstetricia y Enfermería, el cual las citas médicas lo realizan de forma manual. Para mejorar esta situación, realizaremos un sistema de citas, el cual optimizara las atenciones realizadas en el puesto de salud.

## **INTRODUCCIÓN**

En el puesto de salud "Nuevo Trujillo", se encuentra Ubicado en el Departamento de Cajamarca, provincia de San Ignacio, distrito San José De Lourdes, el registro de las citas se realiza de forma manual, todo este registro va en un cuaderno. Debido a esto, en muchos días suele a ver demasiado pacientes citados, ya que, al no haber un control del horario, dan algunas citas médicas en el mismo horario de otros pacientes.

Por ello el puesto de salud "Nuevo Trujillo", tiene problemas al momento de generar citas médicas, porque, al realizarlo manualmente, se demoran en registrar, se equivocan en los datos del paciente, registran citas en el mismo horario de otros pacientes, todo esto genera demora y también en la atención médica, porque en muchos casos se pierden las hojas de las citas generadas. Todo esto causa desorden en la atención e incomodidad a los pacientes.

Es por ello que se implementará un sistema de citas, la cual ayudará a automatizar todos estos procesos, las cuales tendrá como objetivo:

- Minimizar el tiempo del registro de citas médicas.
- Tener un mayor orden en el horario de citas.
- Atender las citas con mayor rapidez.
- Realizar la consulta de la cita.
- Generar reportes de las citas atendidas.

Todo esto conlleva a que el puesto de Salud Nuevo Trujillo, mejore en su atención, el cual será más rápida y fluida, y así los pacientes puedan ser mejor atendidos.

## **1. ESTUDIO DE FACTIBILIDAD**

El presente proyecto tiene como objetivo medir la factibilidad que tiene la posta médica “Nuevo Trujillo” implicando el funcionamiento del área de citas médicas y también, a su vez acelerando el proceso de atención de los pacientes y el efecto que tiene en la población.

### **1.1. Factibilidad operativa y técnica: La visión del sistema**

El documento de la Visión debe ser preparado de acuerdo al análisis de funcionamiento sugerido por el personal que labora en la posta médica “Nuevo Trujillo”. Las consultas recogidas ayudaran al desarrollo de un sistema óptimo para el área de citas, como requisito se demarca que debe de haber un buen orden para la atención adecuada de la salud de cada paciente, ya que de ello depende la reputación de la institución y el bienestar de la población.

## **2. MODELO DEL NEGOCIO**

![Diagrama de proceso de negocio de Eriksson y Penker](https://bitbucket.org/jhef-r/ads_reposalud/raw/1e4efe64623c1d60b1011ccaefb66227918f81a1/Image/Process%20Model.png)

Figura No. 1. Modelo del proceso de negocio Eriksson y Penker

</br>

![Diagrama de proceso de negocio en Bizagi](https://bitbucket.org/jhef-r/ads_reposalud/raw/06b0e0bcefcd5e28ab89f3ae05f8a4768cb4912d/Image/Bizagi.png)

Figura No. 2. Modelo del proceso de negocio Bizagi

</br>

### **2.1 MODELO DE CASO DE USO DEL NEGOCIO**

#### **2.1.1. LISTA DE LOS ACTORES DEL NEGOCIO**

Lista de actores del negocio |  | ![AN][Actor] |
---|---|---|
Nombre | Descripción  |
AN1_Paciente | Persona que solicita una cita médica. |
AN2_Enfermera | Persona que registra la cita médica del paciente.
AN3_Médico | Persona que atiende la cita médica del paciente.

Tabla No. 1 - Lista de Actores del Negocio

![AN](https://bitbucket.org/jhef-r/ads_reposalud/raw/bc821d39d09c36bc162504993346a2f75f0a90ff/Image/AN.png)

Figura No. 3. Lista de actores del negocio (AN)

#### **2.1.2. LISTA DE CASOS DE USO DEL NEGOCIO**

| Lista de casos de  uso del negocio |  | ![AN][Caso] |
---|---|---|
Nombre | Descripción  |
CUN1_Solicitar cita médica | El paciente solicita una cita médica.
CUN2_Registrar cita médica | La enfermera registra una cita médica.
CUN3_Registrar paciente | Durante el proceso del CUN2, se registra al paciente.
CUN4_Asignar médico | Durante el proceso del CUN2, se asigna al médico.
CUN5_ Atender paciente | El médico atiende al paciente en su consultorio y le pide su cita médica.
CUN6_Consultar cita médica | El médico consulta la cita médica que se le pidió en el CUN5.
CUN7_Generar reporte | El médico a fin de mes genera un reporte.

Tabla No. 2 - Lista de Casos de Uso del Negocio

![CUN](https://bitbucket.org/jhef-r/ads_reposalud/raw/bc821d39d09c36bc162504993346a2f75f0a90ff/Image/CUN.png)

Figura No. 4. Lista de Casos de Uso del Negocio (CUN)

#### **2.1.3. DIAGRAMA DE CASOS DE USO DEL NEGOCIO**

![DCUN](https://bitbucket.org/jhef-r/ads_reposalud/raw/bc821d39d09c36bc162504993346a2f75f0a90ff/Image/DCUN.png)

Figura No. 5. Diagrama de Casos de Uso del Negocio (DCUN)

#### **2.1.4. ESPECIFICACIONES DE CASOS DE USO DEL NEGOCIO**

Especificación de alto nivel

Nombre | CUN1_Solicitar cita médica |
---|---|
Descripción | El paciente al llegar al puesto de salud “Nuevo Trujillo”, se acerca a la enfermera en la recepción y solicita una cita médica, este proceso termina cuando le atiende la enfermera.
Actores de negocio | Paciente, Enfermera
Entradas | Solicitar cita médica
Entregables | Registrar cita médica
Mejoras | Este proceso se realizará en el sistema y ya no en hojas.

Tabla No. 3. –  ECUN – Alto nivel

</br>

Nombre | CUN2_Registrar cita médica |
---|---|
Descripción | La enfermera atiende al paciente y procede a registrar su cita, este proceso acaba cuando esta realizado el registro médico.
Actores de negocio | Paciente, Enfermera
Entradas | Registrar cita médica, Registrar paciente, Asignar médico
Entregables | Registrar cita médica
Mejoras | Este proceso se realizará en el sistema y ya no en hojas.

Tabla No. 4. –  ECUN – Alto nivel

</br>

Nombre | CUN3_Registrar paciente |
---|---|
Descripción | La enfermera pide los datos del paciente para registrarlo, este proceso termina cuando se registra al paciente satisfactoriamente.
Actores de negocio | Paciente, Enfermera
Entradas | Registrar paciente
Entregables | Registrar paciente
Mejoras | Este proceso se realizará en el sistema y ya no en hojas.

Tabla No. 5. –  ECUN – Alto nivel

</br>

Nombre | CUN4_Asignar médico |
---|---|
Descripción | La enfermera asigna al médico correspondiente, este proceso termina cuando la asignación se realiza satisfactoriamente
Actores de negocio | Médico, Enfermera
Entradas | Asignar médico
Entregables | Asignar médico
Mejoras | Este proceso se realizará en el sistema y ya no en hojas.

Tabla No. 6. –  ECUN – Alto nivel

</br>

Nombre | CUN5_ Atender paciente |
---|---|
Descripción | El paciente pasa al consultorio del médico para ser atendido, le pide su cita médica, este proceso termina cuando el paciente se retira del consultorio.
Actores de negocio | Médico, Paciente
Entradas | Atender paciente
Entregables | Atender paciente
Mejoras | Este proceso se realizará en el sistema y ya no en hojas.

Tabla No. 7. –  ECUN – Alto nivel

</br>

Nombre | CUN6_Consultar cita médica |
---|---|
Descripción | El médico consulta sobre que se le atenderá, este proceso termina cuando el doctor obtiene información del registro de cita médica.
Actores de negocio | Médico
Entradas | Consultar cita médica
Entregables | Consultar cita médica
Mejoras | Este proceso se realizará en el sistema y ya no en hojas.

Tabla No. 8. –  ECUN – Alto nivel

</br>

Nombre | CUN7_Generar reporte |
---|---|
Descripción | El médico cada fin de mes realiza un reporte de las atenciones realizadas, este proceso termina cuando se genera todos los reportes satisfactoriamente.
Actores de negocio | Médico
Entradas | Generar reporte
Entregables | Generar reporte
Mejoras | Este proceso se realizará en el sistema y ya no en hojas.

Tabla No. 9. –  ECUN – Alto nivel

### **2.2 MODELO DE ANÁLISIS DE NEGOCIO**

#### **2.2.1. LISTA DE TRABAJADORES DE NEGOCIO**

Lista de trabajadores de negocio |  | ![TN][TN] |
---|---|---
Nombre | Descripción  |
TN1_Enfermera | Persona que atiende al paciente y registra las citas médicas.
TN2_Médico | Persona que atiende al paciente sobre su cita médica.

Tabla No. 10. Trabajadores del Negocio

![TN](https://bitbucket.org/jhef-r/ads_reposalud/raw/bc821d39d09c36bc162504993346a2f75f0a90ff/Image/TN.png)

Figura No. 6. Trabajadores del Negocio

#### **2.2.2. LISTA DE ENTIDADES DE NEGOCIO**

Lista de entidades de negocio |  |  | ![EN][EN] |
---|---|---|---
Nombre | Descripción | Origen | Tipo
EN1_Cita médica | Registro de todas las citas médicas. | I | P
EN2_Paciente | Registro de todos los pacientes | I | P
EN3_Reporte | Registro de todos los reportes realizados. | I | P

Tabla No. 11. Entidades del Negocio

</br>

![EN](https://bitbucket.org/jhef-r/ads_reposalud/raw/bc821d39d09c36bc162504993346a2f75f0a90ff/Image/EN.png)

Figura No. 7. Entidades del Negocio

</br>

#### **2.2.3. REALIZACIÓN DE CASOS DE USO DEL NEGOCIO**

![RCUN](https://bitbucket.org/jhef-r/ads_reposalud/raw/bc821d39d09c36bc162504993346a2f75f0a90ff/Image/RCUN.png)

Figura No. 8. Realización de Casos de Uso del Negocio

</br>

#### **2.2.4. DIAGRAMA DE ACTIVIDADES**

![DA](https://bitbucket.org/jhef-r/ads_reposalud/raw/bc821d39d09c36bc162504993346a2f75f0a90ff/Image/DA.png)

Figura No. 9. Diagrama de Actividad

</br>

#### **2.2.5. REALIZACIÓN DE CLASES DE DOMINIO**

![DCD](https://bitbucket.org/jhef-r/ads_reposalud/raw/964ed66977e8affd02ef0d6924a7932a035cfb90/Image/DCD.png)

Figura No. 10. Diagrama de Clases de Dominio

</br>

### **2.3. GLOSARIO DE TÉRMINOS**

Nombre | Descripción |
---|---|
AN | Actor de Negocio
CUN | Casos de Uso del Negocio
DCUN | Diagrama de Casos de Uso de Negocio
TN | Trabajador del Negocio
EN | Entidad del Negocio
RCUN | Realización de Casos de Negocio
DA | Diagrama de Actividad
DCD | Diagrama de Clases de Dominio
RN | Regla de Negocio

Tabla No. 12. Glosario de Términos

</br>

### **2.4. REGLAS DE NEGOCIO**

Código | Nombre | Descripción | Casos de Uso Afectados
---|---|---|---
| | |Reglas de Hechos
RN01 | Regla 01 | Para poder registrar una cita médica, el paciente debe tener su DNI físico. | CUN2_Registrar cita médica
RN02 | Regla 02 | Para poder registrar una cita médica, el paciente debe ser registrado. | CUN2_Registrar cita médica, CUN3_Registrar paciente
RN03 | Regla 03 | Para que el paciente sea atendido por el médico tiene que tener su DNI físico y haber sacado su cita médica. | CUN5_Atender paciente
| | |Reglas de Restricción
RN04 | Regla 04 | Si el paciente no tiene su DNI físico, no podrá sacar su cita médica o ser atendido por el médico. | CUN2_Registrar cita médica, CUN5_Atender paciente

Tabla No. 13. Reglas del Negocio

## **3. CAPTURA DE REQUERIMIENTOS**

Captura de los requerimientos para el desarrollo del sistema de control de citas médicas:
Actualmente se maneja un cuaderno de apuntes donde se lleva el control de los pacientes, el cual es un proceso de actividad muy obsoleto. Por ser un medio físico, consume demasiado tiempo en obtener la información, perdida de los datos de los pacientes, requiere espacios físicos para guardar los cuadernos. Pacientes y trabajadores presentan muchas dificultades. Por consiguiente, se implementará un sistema con los siguientes requerimientos:
Se requiere guardar la información de los pacientes en una base de datos por medio de un sistema web, el cual permitirá que los trabajadores de la posta médica, por medio de un dispositivo interactúen con la información de los datos en tiempo real (registro de pacientes, registrar citas médicas, consular los datos de un paciente, generar reportes, etc.). La implementación del sistema mejorar el servicio de gestión y control de la información de los pacientes.

### **3.1. FUENTES DE OBTENCIÓN DE REQUERIMIENTOS**

#### **3.1.1. INFORME DE ENTREVISTAS O MATERIAL TÉCNICO**

Nombre de la institución: PUESTO DE SALUD NUEVO TRUJILLO

La presente entrevista al usuario tiene como objetivo recoger información para determinar los requerimientos y la información que necesita procesar en tiempo real.

INSTRUCCIONES: Responda según sea conveniente para usted y conteste verazmente las preguntas.

1. ¿Qué procesos realizas manualmente que necesiten que se automaticen?

   * Registrar las citas médicas.
   * Registrar a los pacientes.
   * Registrar a los médicos.
   * Registrar su área médica.
   * Registrar los horarios de los médicos.
   * Buscar en el cuaderno la lista de los pacientes.
   * Entregar un ticket con la fecha de la cita médica.

2. ¿Qué necesita que realice el sistema?

    Necesitamos que realice los procesos indicados en la pregunta 1, de una manera ágil y eficiente, todo en tiempo real.

3. ¿Qué tipos de reportes necesitas generar los fines de mes?

   * Lista de pacientes atendidos durante el mes.
   * Lista de pacientes atendidos por cada doctor.
   * Lista de cantidad de veces que se atendió un paciente.

4. ¿Qué beneficios cree Ud. que tendría el automatizar los procesos mencionados?

    Los principales beneficios seria que podría atender más rápido a los pacientes y esto también le beneficia al paciente, porque ya no tendría que esperar largas horas para sacar su cita. A su vez podríamos tener un listado de lo que necesitamos de manera inmediata algo que nos tomaba varias horas en realizarlo.

#### **3.1.2. MATRIZ DE ACTIVIDADES Y REQUERIMIENTOS**

Sistemas de Citas Médicas

Procesos del negocio | Actividad del negocio | Responsables del negocio | Requerimiento | Casos de uso del sistema | Actores del sistema
---|---|---|---|---|---
Registrar citas médicas |  El paciente solicita un cita médica, la enfermera toma sus datos y lo registra en su cuaderno | Enfermera | Registrar Usuario | CUS1_Autenticar usuario | AS1_Enfermera
...| Busca en el cuaderno la lista de los pacientes | Médico | Registrar cita médica RF-002 | CUS2_Registrar usuario | AS2_Médico
...| Busca en el cuaderno la lista de los médicos |...| Registrar paciente RF-003 | CUS3_Registrar cita médica |...
...| Entrega un ticket con la fecha de la cita |...| Registrar médico RF-004 | CUS4_Registrar paciente |...
...|...|...| Registrar ára médica RF-005 | CUS5_Registrar médico |...
...|...|...| Registrar horario RF-006 | CUS6_Registrar área médica |...
...|...|...| Generar reporte RF-007 | CUS7_Registrar horario |...
...|...|...|...| CUS8_Actualizar cita médica |...
...|...|...|...| CUS9_Generar Reporte

Tabla No. 14. Matriz de actividades y requerimientos

#### **3.1.3. TRAZABILIDAD DE REQUISITOS**

Relación | AS1_Enfermera | AS2_Doctor
---|:---:|:---:
CUS1_Auteticar Usuario | x
CUS2_Registrar Usuario | | x
CUS3_Registrar cita médica | x
CUS4_Registrar paciente | x
CUS5_Registrar Médico | x
CUS6_Registrar Área Médica| x
CUS7_Registrar Horario | x
CUS8_Actualizar Cita Médica | | x
CUS9_Generar Reporte | x | x

Tabla No. 15. Trazabilidad de casis de uso y actores

### **3.2. MODELO DE CASOS DE USO**

#### **3.2.1. LISTA DE ACTORES DEL SISTEMA**

Lista de actores del sistema | |
---|---
Nombre | Descripción
AS1_Enfermera | Persona que interactua con el sistema
AS2_Médico | Persona que interactua con el sistema

Tabla No. 16. Actores del sistema

![AS](Image/AS.png)

Figura No. 11. Actores del sistema

#### **3.2.2. LISTA DE CASOS DE USO DEL SISTEMA**

Lista de casos de uso del sistema | |
---|---
Nombre | Descripción
CUS1_Auteticar Usuario | El sistema autentica el usuario y contraseña ingresado por el actor AS1_Enfermera
CUS2_Registrar Usuario | El Actor AS2_Médico ingresa los datos del usuario que manipulara el sistema y el sistema lo guarda.
CUS3_Registrar cita médica | El Actor AS1_Enfermera ingresa los datos requeridos y el sistema lo guarda.
CUS4_Registrar paciente | El Actor AS1_Enfermera ingresa los datos requeridos y el sistema lo guarda.
CUS5_Registrar Médico | El Actor AS1_Enfermera ingresa los datos requeridos y el sistema lo guarda.
CUS6_Registrar Área Médica| El Actor AS1_Enfermera ingresa los datos requeridos y el sistema lo guarda.
CUS7_Registrar Horario | El Actor AS1_Enfermera ingresa los datos requeridos y el sistema lo guarda.
CUS8_Actualizar Cita Médica | El sistema actualiza la cita médica como atendido, despues de que el Actor AS2_Médico realiza el proceso.
CUS9_Generar Reporte | El sistema genera reportes de acuerdo a las consultas realizadas por los actortes AS1_Enfermera o AS2_Médico.

Tabla No. 17. Casos de uso del sistema

![CUS](Image/CUS.png)

Figura No. 12. Casos de uso del sistema

#### **3.2.3. LISTA DE CASOS DE USO PRIORIZADOS**

Priorización de casos de uso del sistema | | | | |
---|---|:---:|:---:|:---:
**Actor** | **Caso de uso** | Alta | Media | Baja
AS1_Enfermera | CUS1_Auteticar Usuario | x
AS2_Médico | CUS2_Registrar Usuario | x
AS1_Enfermera | CUS3_Registrar cita médica | x
AS1_Enfermera | CUS4_Registrar paciente | | x
AS1_Enfermera | CUS5_Registrar Médico | | x
AS1_Enfermera | CUS6_Registrar Área Médica| | x
AS1_Enfermera | CUS7_Registrar Horario | | x
AS2_Médico | CUS8_Actualizar Cita Médica | | | x
AS1_Enfermera</br>AS2_Médico | CUS9_Generar Reporte |  | | x

Tabla No. 18. Lista de casos de uso priorizados

#### **3.2.4. DIAGRAMAS DE CASOS DE USO DEL SISTEMA**

![DCUS](Image/DCUS.png)

Figura No. 13. Diagrama general caso de uso del sistema

#### **3.2.5. ESPECIFICACIONES DE REQUERIMIENTOS DE SOFTWARE**

**Requerimientos Funcionales**

* El sistema verificara la autenticación del usario.
* El sistema permitira registrar, actualizar, eliminar los datos de:
    * Médicos
    * Horarios de médicos
    * Áreas médicas
    * Paciente
    * Citas médicas
    * Usuarios del sistema
* El sistema permitira filtrar los datos de los diferente modulos y poder realizar un reporte de ellos.
* El sistema permitira exportar los reportes a pdf.
* El sistema mostrara un mensaje si al momento de registrar un nuevo dato no cumple con lo requerido.
* El sistema controlara los accesos y solo permitira el ingreso de usuarios autorizados.

**Requerimientos no Funcionales**

* Debe ser fácil de usar, con ayuda e interfaces intuitivas.
* El ingreso al sistema estará restringido con contraseñas cifradas y usuarios definidos.
* El sistema deberá funcionar en distintos tipos de sistemas operativos y plataformas de hardware.
* El sistema debe soportar el manejo de gran cantidad de información durante su proceso.
* El sistema debe proporcionar mensajes de error que sean iformativos y orientados a usuario final.
* El sistema debe ser liviano, no consumir mucho procesador y no podra ocupar mas de 500 Mb de meoria RAM.
* El sistema no podra ocupar mas de 1 Gb de espacio en el disco.

#### **3.2.6. ESPECIFICACIONES DE CASOS DE USO**

Nombre | CUS1_Autenticar Usuario
---|---
Tipo | Alta
Autor | Jhony Chacon
Actores | AS1_Enfermera
Iteración |
CU Relacionado | CUS2_Registrar Usuario
Breve Descripción | Permite validar las credenciales del usuario, para el ingreso al sistema
Referencias |
Flujo de evetos |
Flujo basico | El actor ingresa su usuario y contraseña</br>El actor selecciona la opción iniciar sesión</br>El sistema valida las credenciales en la base de datos</br>El sistema muestra el dashboard de la pagina web
Sub Flujo |
Flujo Alternativo | Las credenciales del actor no existe</br>Error al ingresar el usuario o contraseña
Pre Condición | El actor debe estar registrado en el sistema
Post Condición | Validación realizado con éxito.

Tabla No. 19. Especificación CUS1

</br>

Nombre | CUS2_Registrar Usuario
---|---
Tipo | Alta
Autor | Jhony Chacon
Actores | AS2_Médico
Iteración |
CU Relacionado | CUS1_Autenticar Usuario
Breve Descripción | Permite registrar a los nuevos usuarios
Referencias |
Flujo de evetos |
Flujo basico | El Actor despliega la ventana de usuarios</br>El actor selecciona la opción de nuevo usuario</br>El actor ingresara los datos necesarios al formulario</br>El actor selecciona la opcion guardar</br>El sistema registrara los datos del usuario a la base de datos</br>El sistema muestra la lista de los usuarios
Sub Flujo |
Flujo Alternativo | Los datos del usuario ya existe en el sistema</br>Los datos del usuario no están completos
Pre Condición | El actor debe usar el usario registrado por defecto del sistema
Post Condición | Registro realizado con éxito

Tabla No. 20. Especificación CUS2

</br>

Nombre | CUS3_Registrar Cita Médica
---|---
Tipo | Alta
Autor | Jhef Rojas
Actores | AS1_Enfermera
Iteración |
CU Relacionado | CUS4_Registrar Paciente</br>CUS5_Registrar Médico</br>CUS8_Actualizar Cita Médica
Breve Descripción | Permite registrar nuevas citas médicas
Referencias |
Flujo de evetos |
Flujo basico | El Actor despliega la ventana de citas médicas</br>El actor selecciona la opción de nueva cita médica</br>El actor ingresara los datos necesarios al formulario</br>El actor selecciona la opcion guardar</br>El sistema registrara los datos de la cita médica a la base de datos</br>El sistema muestra la lista de las citas médicas
Sub Flujo |
Flujo Alternativo |Los datos del paciente estan incompletos</br>Los datos del médico estan incompletos
Pre Condición | El actor debe estar registrado en el sistema</br>El paciente debe estar registrado en el sistema</br>El médico debe estar registrado en el sistema
Post Condición | Registro realizado con éxito

Tabla No. 21. Especificación CUS3

</br>

Nombre | CUS4_Registrar Paciente
---|---
Tipo | Media
Autor | Jhef Rojas
Actores | AS1_Enfermera
Iteración |
CU Relacionado | CUS3_Registrar Cita Médica
Breve Descripción | Permite registrar a los nuevos pacientes
Referencias |
Flujo de evetos |
Flujo basico | El Actor despliega la ventana de pacientes</br>El actor selecciona la opción de nuevo paciente</br>El actor ingresara los datos necesarios al formulario</br>El actor selecciona la opcion guardar</br>El sistema registrara los datos del paciente a la base de datos</br>El sistema muestra la lista de los pacientes
Sub Flujo |
Flujo Alternativo | Los datos del paciente ya existe en el sistema</br>Los datos del paciente no están completos
Pre Condición | El actor debe estar registrado en el sistema
Post Condición | Registro realizado con éxito

Tabla No. 22. Especificación CUS4

</br>

Nombre | CUS5_Registrar Médico
---|---
Tipo | Media
Autor | Jhef Rojas
Actores | AS1_Enfermera
Iteración |
CU Relacionado | CUS3_Registrar Cita Médica</br>CUS6_Registrar Área Médica</br>CUS7_Registrar Horario
Breve Descripción | Permite registrar a los nuevos médicos
Referencias |
Flujo de evetos |
Flujo basico | El Actor despliega la ventana de médicos</br>El actor selecciona la opción de nuevo médico</br>El actor ingresara los datos necesarios al formulario</br>El actor selecciona la opcion guardar</br>El sistema registrara los datos del médico a la base de datos</br>El sistema muestra la lista de los médicos
Sub Flujo |
Flujo Alternativo | Los datos del médico ya existe en el sistema</br>Los datos del médico no están completos
Pre Condición | El actor debe estar registrado en el sistema</br>Debe estar registrado el área médica
Post Condición | Registro realizado con éxito

Tabla No. 23. Especificación CUS5

</br>

Nombre | CUS6_Registrar Área Médica
---|---
Tipo | Media
Autor | Jhef Rojas
Actores | AS1_Enfermera
Iteración |
CU Relacionado | CUS5_Registrar Médico
Breve Descripción | Permite registrar a las nuevas áreas médicas
Referencias |
Flujo de evetos |
Flujo basico | El Actor despliega la ventana áreas médicas</br>El actor selecciona la opción de nueva área médica</br>El actor ingresara los datos necesarios al formulario</br>El actor selecciona la opcion guardar</br>El sistema registrara los datos del área médica</br>El sistema muestra la lista de las áreas médicas
Sub Flujo |
Flujo Alternativo | El área médica ya existe en el sistema
Pre Condición | El actor debe estar registrado en el sistema
Post Condición | Registro realizado con éxito

Tabla No. 24. Especificación CUS6

</br>

Nombre | CUS7_Registrar Horario
---|---
Tipo | Media
Autor | Jhef Rojas
Actores | AS1_Enfermera
Iteración |
CU Relacionado | CUS5_Registrar Médico
Breve Descripción | Permite registrar a los nuevos horarios de los médicos
Referencias |
Flujo de evetos |
Flujo basico | El Actor despliega la ventana horarios</br>El actor selecciona la opción de nuevo horario</br>El actor ingresara los datos necesarios al formulario</br>El actor selecciona la opcion guardar</br>El sistema registrara los datos del horario</br>El sistema muestra la lista de los horarios de los médicos
Sub Flujo |
Flujo Alternativo | El horario ya existe en el sistema
Pre Condición | El actor debe estar registrado en el sistema</br>El médico ya debe estar registrado en el sistema
Post Condición | Registro realizado con éxito

Tabla No. 25. Especificación CUS7

</br>

Nombre | CUS8_Actualizar Cita Médica
---|---
Tipo | Baja
Autor | Jhef Rojas
Actores | AS2_Médico
Iteración |
CU Relacionado | CUS3_Registrar Cita Médica
Breve Descripción | Permite actualizar el estado de la cita médica
Referencias |
Flujo de evetos |
Flujo basico | El Actor despliega la ventana citas médicas</br>El actor selecciona la opción pendiente</br>El sistema cambia el estado a atendido</br>El sistema actualiza el estado en la base de datos
Sub Flujo |
Flujo Alternativo |
Pre Condición | El actor debe estar registrado en el sistema</br>La cita médica debe estar generado en el sistema
Post Condición | Actualización realizado con éxito

Tabla No. 26. Especificación CUS8

</br>

Nombre | CUS9_Generar Reporte
---|---
Tipo | Baja
Autor | Jhef Rojas
Actores | AS1_Enfermera</br>AS2_Médico
Iteración |
CU Relacionado |
Breve Descripción | Permite generar reportes
Referencias |
Flujo de evetos |
Flujo basico | El Actor despliega la ventana citas médicas</br>El actor realiza el filtrado para el reporte</br>El sistema muestra la consulta</br>El actor selecciona la opción exportar</br>El sistema exportar el reporte a pdf
Sub Flujo |
Flujo Alternativo |
Pre Condición | El actor debe estar registrado en el sistema
Post Condición | Reporte generado con éxito

Tabla No. 27. Especificación CUS9

### **3.3. PROTOTIPO DE PRINCIPALES INTERFACES**

![Login](Image/Interface/Login.png)

Figura No. 14. Interfaz Login

![Citas](Image/Interface/Citas.png)

Figura No. 15. Interfaz Citas Médicas

![Registrocm](Image/Interface/Registro-de-citas.png)

Figura No. 16. Interfaz Registro de citas médicas

![Paciente](Image/Interface/Paciente.png)

Figura No. 18. Interfaz Paciente

![Registropa](Image/Interface/Regisro-de-paciente.png)

Figura No. 19. Interfaz Registro de pacientes

![Medicos](Image/Interface/Medicos.png)

Figura No. 20. Interfaz Médicos

![Registrome](Image/Interface/Registro-de-medicos.png)

Figura No. 21. Interfaz Registro de médicos


![Areasmedicas](Image/Interface/Areas-medicas.png)

Figura No. 22. Interfaz Áreas médicas

![Registroam](Image/Interface/Registro-de-areas-medicas.png)

Figura No. 23. Interfaz Registro de áreas médicas

![Horarios](Image/Interface/Horarios.png)

Figura No. 24. Interfaz Horarios de los médicos

![Registroh](Image/Interface/Registro-de-horarios.png)

Figura No. 25. Interfaz Registro de Horarios

![Usuarios](Image/Interface/Usuarios.png)

Figura No. 26. Interfaz Usuarios del sistema

![Registrousu](Image/Interface/Registro-de-usuarios.png)

Figura No. 27. Interfaz Registro de usuarios

### **3.4. DIAGRAMA DE CLASES DE DISEÑO**

![ClaseDiseño](Image/Clase%20Diseño/DCD.png)

Figura No. 28. Diagrama de clases de diseño

### **3.5. DIAGRAMA DE PAQUETES**

![pkgMCUN](Image/Package/Packge%20mcun.png)

Figura No. 29. Package Modelo de casos de uso del negocio

![pkgMAN](Image/Package/Packge%20man.png)

Figura No. 30. Package Modelo de analisis de negocio

![pkgDominio](Image/Package/Packge%20dominio.png)

Figura No. 31. Package Modelo de Dominio

![pkgMCUS](Image/Package/Packge%20mcus.png)

Figura No. 32. Modelo de casos de uso del sistema

![pkgClasesDiseño](Image/Package/Packge%20Clase%20de%20Diseño.png)

Figura No. 33. Package Diagrama de clases de diseño

![pkgSecuencia](Image/Package/Packge%20secuencia.png)

Figura No. 34. Package Diagrama de secuencia

![pkgEstado](Image/Package/Packge%20estado.png)

Figura No. 35. Package Diagrama de estado

### **3.6. DIAGRAMA DE SECUENCIA**

![SecLogin](Image/Secuencia/Login.png)

Figura No. 36. Diagrama de secuencia login

![SecMedico](Image/Secuencia/Medico.png)

Figura No. 37. Diagrama de secuencia registrar médico

![SecPaciente](Image/Secuencia/Paciente.png)

Figura No. 38. Diagrama de secuencia registrar paciente

![SecUsuario](Image/Secuencia/Usuario.png)

Figura No. 39. Diagrama de secuencia registrar usuario

![SecHorario](Image/Secuencia/Horario.png)

Figura No. 40. Diagrama de secuencia registrar horario

![SecCitaMedica](Image/Secuencia/Cita%20Medica.png)

Figura No. 41. Diagrama de secuencia registrar cita médica

![SecAreaMedica](Image/Secuencia/Area%20Medica.png)

Figura No. 42. Diagrama de secuencia registrar area médica

![SecActualizarCita](Image/Secuencia/Actualizar%20Cita.png)

Figura No. 43. Diagrama de secuencia actualizar cita

![SecReporte](Image/Secuencia/Reporte.png)

Figura No. 44. Diagrama de secuencia generar reporte

### **3.7. DIAGRAMA DE ESTADO**

![EstRegistrar](Image/Estado/Registrar.png)

Figura No. 45. Diagrama de estado ingresar registro

![EstActualizar](Image/Estado/Actualizar.png)

Figura No. 46. Diagrama de estado actualizar registro

![EstBuscar](Image/Estado/Buscar.png)

Figura No. 47. Diagrama de  estado buscar registro

![EstEliminar](Image/Estado/Eliminar.png)

Figura No. 48. Diagrama de estado eliminar registro

### **CONCLUSIONES**

Luego de haber realizado el análisis y diseño del sistema de administración de pacientes, se lograron obtener las siguientes conclusiones:

1. La implementación del sistema de citas médicas e historias clínicas en el Puesto de Salud “Nuevo Trujillo”, permitió mejorar el proceso de registro de pacientes en el área de admisión, dado que ahora se desarrolla a través de un software asistido por el personal asistencial de admisión, en tiempo real y con el respaldo de un sistema que guarda y respalda la información de los pacientes en forma confidencial. Además, se logró reducir el tiempo invertido en el registro de un paciente, que antes de usar el sistema era de 8 minutos aproximadamente, mientras que, ahora haciendo uso del sistema, este tiempo se redujo a 3 minutos, es decir, se redujo el tiempo en un 60 % aproximadamente, todo esto en beneficio de los pacientes.

2. La implementación del sistema de citas médicas e historias clínicas en el Puesto de Salud “Nuevo Trujillo”, permitió mejorar el proceso de búsqueda de historias clínicas en el área de admisión. Se logró reducir el tiempo invertido en la búsqueda de una historia clínica de un paciente, que antes de usar el sistema era de 10 - 15 minutos aproximadamente, mientras que, ahora haciendo uso del sistema, este tiempo se redujo a 30 segundos, es decir, se redujo el tiempo en un 98 % aproximadamente, todo esto en beneficio de los pacientes.

3. El área de admisión del Puesto de Salud “Nuevo Trujillo”, cuentan ahora con un soporte de copia de seguridad de los datos personales de los pacientes consignados en el proceso de registro. De esta manera se minimizó el riesgo de pérdida de información relevante en las historias clínicas de los pacientes, generándose un beneficio tanto para los pacientes como para el mismo Puesto de Salud.

4. Los médicos del Puesto de Salud “Nuevo Trujillo”, ahora cuentan con una plataforma de consultas médicas e historial totalmente para sus pacientes de diferentes especilidades.

5. El sistema de citas médicas e historias clínicas es un sistema idóneo, además emite reportes de asignación de citas médicas, y registro de pacientes, permitiendo también la optimización el proceso de búsqueda de historias clínicas.

6. El tiempo promedio que tarda un paciente para ser atendido en los consultorios de Medicina general y otras especialidades se han minimizado con el uso del sistema.


...

>>>>>>> eba89444a6b17327a162daf3e2fb0cf283813480

[Actor]:https://bitbucket.org/jhef-r/ads_reposalud/raw/06b0e0bcefcd5e28ab89f3ae05f8a4768cb4912d/Image/Img-cuadro/Imagen1.jpg

[Caso]:https://bitbucket.org/jhef-r/ads_reposalud/raw/06b0e0bcefcd5e28ab89f3ae05f8a4768cb4912d/Image/Img-cuadro/Imagen2.png

[TN]:https://bitbucket.org/jhef-r/ads_reposalud/raw/bc821d39d09c36bc162504993346a2f75f0a90ff/Image/Img-cuadro/trabajador-negocio.jpg

[EN]:https://bitbucket.org/jhef-r/ads_reposalud/raw/4504574cdf8f63a13e244205abd1bd4fab84238f/Image/Img-cuadro/Entidad.jpg